<?php declare(strict_types = 1);

namespace Tests\Cases;

require_once __DIR__ . '/../bootstrap.php';

use Nette\Utils\ArrayHash;
use Snugcomponents\Paginator\Examples\Builders\ArrayDataBuilder;
use Snugcomponents\Paginator\Paginator;
use Mockery;
use Nette\Application\UI\Control;
use Nette\Application\UI\Template;
use Nette\Application\UI\TemplateFactory;
use Nette\Utils\Paginator as NuPaginator;
use Tester\Assert;
use Tester\TestCase;
use function assert;
use function count;

class PaginatorTest extends TestCase
{

    protected function tearDown(): void
    {
        parent::tearDown();
        Mockery::close();
    }

    /**
     * @param        array<int> $data
     * @param        array<int> $expected
     * @dataProvider ../fixtures/PaginatorTestPage.php
     */
    public function testGetPage(
        array $data,
        int $itemsPerPage,
        int $firstPage,
        int $page,
        array $expected,
        int $relatedPages = 3,
    ): void {
        $dataProvider = ArrayDataBuilder::create($data);

        $paginator = new Paginator($dataProvider, $itemsPerPage, $firstPage, $relatedPages);
        $paginator->page = $page;

        Assert::equal($expected, $paginator->applyPaginator()->build(), 'testGetPage failed');
    }

    /**
     * @param        array<int>    $data
     * @param        array<string> $expected
     * @dataProvider ../fixtures/PaginatorTestRender.php
     */
    public function testRender(
        array $data,
        int $itemsPerPage,
        int $firstPage,
        array $expected,
        string $filePath,
        int $relatedPages = 3,
    ): void {
        $dataProvider = ArrayDataBuilder::create($data);

        $paginatorControl = new Paginator($dataProvider, $itemsPerPage, $firstPage, $relatedPages);

        $paginatorControl->setTemplateFile($filePath);

        $templateFactory = $this->getTemplateFactory($filePath);

        $paginatorControl->setTemplateFactory($templateFactory);

        $paginator = new NuPaginator();
        $paginator->setItemsPerPage($itemsPerPage);
        $paginator->setBase($firstPage);
        $paginator->setItemCount(count($data));
        $paginator->setPage(1);

        $paginatorControl->render();

        $template = $templateFactory->createTemplate();

        Assert::equal($paginator, $template->paginator, 'testRender failed');
        Assert::same($expected, $template->steps, 'testRender failed');
    }

    public function testRender2(): void
    {
        $dataProvider = ArrayDataBuilder::create([]);

        $paginator = new Paginator($dataProvider, itemsPerPage: 0);

        $filePath = dirname(__DIR__, 2) . DIRECTORY_SEPARATOR . 'src/Examples/bootstrap4.latte';

        $templateFactory = $this->getTemplateFactory($filePath);

        $paginator->setTemplateFactory($templateFactory);

        $paginator->render();

        Assert::true(true);
    }

    private function getTemplateFactory(string $filePath): TemplateFactory
    {
        return new class($filePath) implements TemplateFactory {

            private Template $templateMock;

            public function __construct(private string $filePath,)
            {
                $this->templateMock = $this->getTemplateMock();
            }

            public function createTemplate(Control|null $control = null): Template
            {
                return $this->templateMock;
            }

            private function getTemplateMock(): Template
            {
                 $retVal = Mockery::mock(Template::class)
                     ->shouldReceive('render')
                     ->once()
                     ->with($this->filePath)
                     ->getMock();
                 assert($retVal instanceof Template);

                 return $retVal;
            }

        };
    }

}

(new PaginatorTest())->run();
