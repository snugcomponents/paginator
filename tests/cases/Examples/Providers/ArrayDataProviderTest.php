<?php declare(strict_types = 1);

namespace Tests\Cases\Examples\Providers;

require_once __DIR__ . '/../../../bootstrap.php';

use Snugcomponents\Paginator\Examples\Builders\ArrayDataBuilder;
use Nette\Utils\Paginator as NuPaginator;
use Tester\Assert;
use Tester\TestCase;

class ArrayDataProviderTest extends TestCase
{

    public function testCreate(): void
    {
        $dataProviderCreate = ArrayDataBuilder::create([]);
        $dataProviderNew = new ArrayDataBuilder([]);

        Assert::equal($dataProviderNew, $dataProviderCreate, 'testCreate failed');
    }

    /**
     * @param        array<int> $data
     * @param        array<int> $expected
     * @dataProvider ../../../fixtures/Examples/Providers/ArrayDataProviderTestPage.php
     */
    public function testPage(
        array $data,
        int $itemsPerPage,
        int $firstPage,
        int $page,
        int $expectedItemCount,
        array $expected,
    ): void {
        $dataProvider = ArrayDataBuilder::create($data);

        $paginator = new NuPaginator();
        $paginator->setItemsPerPage($itemsPerPage);
        $paginator->setBase($firstPage);
        $paginator->setPage($page);

        Assert::equal($expected, $dataProvider->page($paginator)->build(), 'testPage failed');
        Assert::same($expectedItemCount, $paginator->getItemCount(), 'testPage failed');
    }

}

(new ArrayDataProviderTest())->run();
