<?php declare(strict_types = 1);

namespace Snugcomponents\Paginator;

use Countable;
use Nette\Application\Attributes\Persistent;
use Nette\Application\UI\Control;
use Nette\Utils\Arrays;
use Nette\Utils\Paginator as NuPaginator;
use Traversable;
use function array_unique;
use function array_values;
use function max;
use function min;
use function range;
use function round;
use function sort;

class Paginator extends Control
{

    #[Persistent]
    public int $page = 1;
    private string $templateFile = __DIR__ . '/Examples/bootstrap4.latte';
    private NuPaginator $paginator;
    /**
     * @var array<callable(self): void>  Occurs when page signal is received
     */
    public $onPagination = [];

    public function __construct(
        private readonly PaginatorDataBuilder $dataBuilder,
        int                                   $itemsPerPage,
        int                                   $firstPage = 1,
        private int                           $relatedPages = 3,
    ) {
        $this->paginator = new NuPaginator();
        $this->paginator->setItemsPerPage($itemsPerPage);
        $this->paginator->setBase($firstPage);

        if ($this->relatedPages < 0) {
            $this->relatedPages = 0;
        }
    }

    public function render(): void
    {
        $this->applyPaginator(); // Because of getting last page info

        $this->template->paginator = $this->paginator;
        $this->template->steps = $this->getSteps();

        $this->template->render($this->templateFile);
    }

    public function setTemplateFile(string $file): void
    {
        $this->templateFile = $file;
    }

    public function applyPaginator(): PaginatorDataBuilder
    {
        if (!isset($this->paginated)) {
            $this->paginator->setPage($this->page);
            $this->dataBuilder->page($this->paginator);
        }

        return $this->dataBuilder;
    }

    public function handlePage(): void
    {
        Arrays::invoke($this->onPagination, $this);
		$this->redrawControl('paginator');
    }

    /**
     * @return array<int>
     */
    private function getSteps(): array
    {
        $pageCount = $this->paginator->getPageCount();
        $page = $this->paginator->getPage();
        $firstPage = $this->paginator->getFirstPage();
        $lastPage = $this->paginator->getLastPage();

        if ($pageCount < 2) {
            $steps = [$page];
        } else {
            $arr = range(
                (int) max($firstPage, $page - $this->relatedPages),
                (int) min($lastPage, $page + $this->relatedPages),
            );
            $count = 4;
            $quotient = ($pageCount - 1) / $count;

            for ($i = 0; $i <= $count; $i++) {
                $arr[] = (int) (round($quotient * $i) + $firstPage);
            }

            sort($arr);

            $steps = array_values(array_unique($arr));
        }

        return $steps;
    }

}
