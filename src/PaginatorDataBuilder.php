<?php declare(strict_types = 1);

namespace Snugcomponents\Paginator;

use Nette\Utils\Paginator;

interface PaginatorDataBuilder
{

    public function page(Paginator $paginator): static;

}
