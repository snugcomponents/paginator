<?php declare(strict_types = 1);

namespace Snugcomponents\Paginator;

use Nette\DI\CompilerExtension;

class PaginatorExtension extends CompilerExtension
{

    public function loadConfiguration(): void
    {
        $this->compiler->loadDefinitionsFromConfig(
            $this->loadFromFile(__DIR__ . '/config/common.neon')['services'],
        );
    }

}
