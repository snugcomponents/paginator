<?php declare(strict_types = 1);

namespace Snugcomponents\Paginator\Examples\Builders;

use Snugcomponents\Paginator\PaginatorDataBuilder;
use Nette\SmartObject;
use Nette\Utils\ArrayHash;
use Nette\Utils\Paginator;
use function array_chunk;
use function count;

class ArrayDataBuilder implements PaginatorDataBuilder
{

    use SmartObject;

    private ?Paginator $paginator = null;

    /**
     * @param array<mixed> $data
     */
    public function __construct(private array $data)
    {
    }

    /**
     * @param array<mixed> $data
     */
    public static function create(array $data): self
    {
        return new self($data);
    }

    /**
     * Page called by the extension
     *
     * In the builder, all methods should only set builder instance.
     * So this method only sets the paginator.
     *
     * Real application of paginator should be done in build() method
     */
    public function page(Paginator $paginator): static
    {
        $this->paginator = $paginator;

        // This needs to be called, because of paginator needs info about last page number
        $this->paginator->setItemCount(count($this->data));

        return $this;
    }

    /**
     * Build method should be used in all builders.
     * This one applies the paginator and returns the data.
     *
     * Remember to call the paginating method and other builder methods in the build method.
     * This provides you flexibility when you need to change settings, and it is lazy,
     * so when you don't call the build method, then paginating will not execute.
     *
     * @return array<mixed>
     */
    public function build(): array
    {
        $this->innerPage();
        return $this->data;
    }

    /**
     * Because of laziness
     */
    private function innerPage(): void
    {
        if ($this->paginator === null) {
            return;
        }

        $chunks = array_chunk($this->data, max(1, $this->paginator->getItemsPerPage()), true);

        $this->data = $chunks[$this->paginator->getPage() - $this->paginator->getFirstPage()] ?? [];
    }

}
