<?php declare(strict_types = 1);

namespace Snugcomponents\Paginator\Examples\Builders;

use Snugcomponents\Paginator\PaginatorDataBuilder;
use Countable;
use Nette\Database\Table\Selection;
use Nette\SmartObject;
use Nette\Utils\Paginator;
use Traversable;

class NetteDatabaseExplorerDataBuilder implements PaginatorDataBuilder
{

    use SmartObject;

    public function __construct(private Selection $selection)
    {
    }

    public static function create(Selection $selection): self
    {
        return new self($selection);
    }

    /**
     * Page called by the extension
     *
     * In this case you can see paginating login right in this function.
     * This can be done, because that Selection is already builder,
     * so if you call these methods, then Selection will take care of laziness.
     */
    public function page(Paginator $paginator): static
    {
        $numOfItems = $this->selection->count('*');
        $paginator->setItemCount($numOfItems);

        $this->selection = $this->selection
            ->limit(
                $paginator->getLength(),
                $paginator->getOffset(),
            );

        return $this;
    }

    /**
     * Build method should be used in all builders.
     * This one only returns the data.
     *
     * In this case you don't need to call page at the end in build method.
     * This can be done because Selection is already a builder,
     * which executes query only if some fetch method is called, or it is argument of foreach or count.
     *
     * So this Builder is only Adapter on Selection
     */
    public function build(): Selection
    {
        return $this->selection;
    }

}
